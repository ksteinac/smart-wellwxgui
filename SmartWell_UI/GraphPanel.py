import os
import threading
import pprint
import random
import sys
import wx

# The recommended way to use wx with mpl is with the WXAgg
# backend.
#
import matplotlib
matplotlib.use('WXAgg')
from matplotlib.figure import Figure
from matplotlib.backends.backend_wxagg import \
    FigureCanvasWxAgg as FigCanvas, \
    NavigationToolbar2WxAgg as NavigationToolbar
import numpy as np
import pylab


class GraphPanel(wx.Panel):
    """ The main frame of the application
    """
    title = 'Demo: dynamic matplotlib graph'

    def __init__(self, parent):
        wx.Panel.__init__(self, parent = parent)

        self.data = []

        self.create_main_panel()

        self.redraw_timer = wx.Timer(self)
        self.Bind(wx.EVT_TIMER, self.on_redraw_timer, self.redraw_timer)
        self.redraw_timer.Start(1000)
        #threadtestGraph = testGraphThread(9, "threadtestGraph", self)
        #threadtestGraph.start()

    def create_main_panel(self):


        self.init_plot()

        self.wellEnableStrList = ["Well 1", "Well 2", "Well 3", "Well 4"]
        self.enableGraphWells = wx.CheckListBox(self, choices = self.wellEnableStrList, size = (100,30))

        self.canvas = FigCanvas(self, -1, self.fig)

        self.toolbar = NavigationToolbar(self.canvas)

        # Remove the forward button
        self.toolbar.DeleteToolByPos(6)

        self.cb_grid = wx.CheckBox(self, -1, "Show Grid", style=wx.ALIGN_RIGHT)
        self.Bind(wx.EVT_CHECKBOX, self.on_cb_grid, self.cb_grid)
        self.cb_grid.SetValue(True)

        self.cb_xlab = wx.CheckBox(self, -1, "Show X labels", style=wx.ALIGN_RIGHT)
        self.Bind(wx.EVT_CHECKBOX, self.on_cb_xlab, self.cb_xlab)
        self.cb_xlab.SetValue(True)

        self.vbox = wx.BoxSizer(wx.VERTICAL)
        self.vbox.Add(self.canvas, 1, flag=wx.LEFT | wx.TOP | wx.GROW)
        # self.vbox.Add(self.toolbar, 0, wx.GROW)

        self.vbox.AddSpacer(10)

        self.hbox1 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox1.Add(self.toolbar, 0, wx.GROW)
        self.hbox1.AddSpacer(10)
        self.hbox1.Add(self.cb_grid, border=5, flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL)
        self.hbox1.AddSpacer(10)
        self.hbox1.Add(self.cb_xlab, border=5, flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL)
        self.hbox1.AddSpacer(10)
        self.hbox1.Add(self.enableGraphWells, flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL)

        self.vbox.Add(self.hbox1, 0, flag=wx.ALIGN_LEFT | wx.TOP)

        self.SetSizerAndFit(self.vbox)
        self.vbox.Fit(self)

        # Init CheckBox
        if self.cb_grid.IsChecked():
            self.axes.grid(True, color='gray')
        else:
            self.axes.grid(False)
        pylab.setp(self.axes.get_xticklabels(),visible=self.cb_xlab.IsChecked())

    def init_plot(self):
        #self.dpi = 100
        self.fig = Figure((10, 2.75))

        self.axes = self.fig.add_subplot(111)
        #self.axes.set_axis_bgcolor('black')
        self.axes.set_title('Graphs', size=14)

        pylab.setp(self.axes.get_xticklabels(), fontsize=8)
        pylab.setp(self.axes.get_yticklabels(), fontsize=8)

        # plot the data as a line series, and save the reference
        # to the plotted line series
        #
        self.plot_data = self.axes.plot(self.data, linewidth=1,color=(1, 1, 0))[0]

    def draw_plot(self):
        """ Redraws the plot
        """
        # when xmin is on auto, it "follows" xmax to produce a
        # sliding window effect. therefore, xmin is assigned after
        # xmax.
        #
        # if self.xmax_control.is_auto():
        #     xmax = len(self.data) if len(self.data) > 50 else 50
        # else:
        #     xmax = int(self.xmax_control.manual_value())
        #
        # if self.xmin_control.is_auto():
        #     xmin = xmax - 50
        # else:
        #     xmin = int(self.xmin_control.manual_value())
        #
        # # for ymin and ymax, find the minimal and maximal values
        # # in the data set and add a mininal margin.
        # #
        # # note that it's easy to change this scheme to the
        # # minimal/maximal value in the current display, and not
        # # the whole data set.
        # #
        # if self.ymin_control.is_auto():
        #     ymin = round(min(self.data), 0) - 1
        # else:
        #     ymin = int(self.ymin_control.manual_value())
        #
        # if self.ymax_control.is_auto():
        #     ymax = round(max(self.data), 0) + 1
        # else:
        #     ymax = int(self.ymax_control.manual_value())
        #
        # self.axes.set_xbound(lower=xmin, upper=xmax)
        # self.axes.set_ybound(lower=ymin, upper=ymax)
        #
        # anecdote: axes.grid assumes b=True if any other flag is
        # given even if b is set to False.
        # so just passing the flag into the first statement won't
        # work.
        #
        #
        #
        # Using setp here is convenient, because get_xticklabels
        # returns a list over which one needs to explicitly
        # iterate, and setp already handles this.



        self.plot_data.set_xdata(np.arange(len(self.data)))
        self.plot_data.set_ydata(np.array(self.data))

        self.canvas.draw()


    def on_cb_grid(self, event):
        if self.cb_grid.IsChecked():
            self.axes.grid(True, color='gray')
        else:
            self.axes.grid(False)
        self.draw_plot()

    def on_cb_xlab(self, event):
        pylab.setp(self.axes.get_xticklabels(),visible=self.cb_xlab.IsChecked())
        self.draw_plot()


    def on_save_plot(self, event):
        file_choices = "PNG (*.png)|*.png"

        dlg = wx.FileDialog(
            self,
            message="Save plot as...",
            defaultDir=os.getcwd(),
            defaultFile="plot.png",
            wildcard=file_choices,
            style=wx.SAVE)

        if dlg.ShowModal() == wx.ID_OK:
            path = dlg.GetPath()
            self.canvas.print_figure(path, dpi=self.dpi)
            self.flash_status_message("Saved to %s" % path)

    def on_redraw_timer(self, event):
        # if paused do not add data, but still redraw the plot
        # (to respond to scale modifications, grid change, etc.)
        #
        #if not self.paused:
        #    self.data.append(self.datagen.next())

        self.draw_plot()

    def on_exit(self, event):
        self.Destroy()

    def flash_status_message(self, msg, flash_len_ms=1500):
        self.statusbar.SetStatusText(msg)
        self.timeroff = wx.Timer(self)
        self.Bind(
            wx.EVT_TIMER,
            self.on_flash_status_off,
            self.timeroff)
        self.timeroff.Start(flash_len_ms, oneShot=True)

    def on_flash_status_off(self, event):
        self.statusbar.SetStatusText('')


if __name__ == '__main__':
    pass
