
'''
This module launches the main program it is also in charge of setting up the serial link and receiving data from the device. It also does data back up.
'''
import sys
import os
import psutil
import glob
import wx
import serial       #need to install pyserial library
import time         #builtin library
import threading    #used to allow user input without stalling processor
from ButtonsStatus import ButtonsStatus
from ConfigPanel import ConfigPanel
from MockUp import TestPanel
global FTDISerial   #create instance of serial connection (change comXX to match port number of Due in device manager)
from WellSelectPanel import WellSelectPanel
global serialLock #lock for serial to prevent from having multiple threads use at same time.

class MainFrame(wx.Frame):

    def __init__(self, *args, **kw):
        super(MainFrame, self).__init__(*args, **kw)

        # Set up Panels test panel is main page and configPanel panel is where events are set up.
        self.testPanel = TestPanel(self, self.GetSize(), FTDISerial)
        self.configPanel = ConfigPanel(self, self.GetSize(), FTDISerial)

        self.testPanel.Show()  # start with test panel up.
        self.configPanel.Hide()
        # wall clock counter.
        self.clock_timer = wx.Timer(self)
        # self.Bind(wx.EVT_TIMER, self.on_clock_timer, self.clock_timer)
        self.clock_timer.Start(1000)
        self.clockBackupcounter = 0

        # create a menu bar

        self.comMenu = wx.Menu()
        self.viewMenu = wx.Menu()
        fileMenu = wx.Menu()
        exitItem = fileMenu.Append(wx.ID_EXIT)
        helpMenu = wx.Menu()

        menuBar = wx.MenuBar()
        menuBar.Append(fileMenu, "&File")
        menuBar.Append(self.comMenu, "&Ports")
        menuBar.Append(self.viewMenu, "&View")
        menuBar.Append(helpMenu, "&Help")
        testItem = self.viewMenu.Append(-1, "&Test")
        ConfigItem = self.viewMenu.Append(-1, "&Event")
        cmdItem = helpMenu.Append(-1, "&Command Help...\tCtrl-H")
        resetComItem = self.comMenu.Append(-1, "Reset COM Ports")
        self.SetMenuBar(menuBar)

        # function binds... This binds a function to a button.
        #self.Bind(wx.EVT_MENU, self.OnCmdHelp, cmdItem)
        self.Bind(wx.EVT_MENU, self.OnTest, testItem)
        self.Bind(wx.EVT_MENU, self.OnConfig, ConfigItem)
        self.Bind(wx.EVT_MENU, self.comReset, resetComItem)
        #self.Bind(wx.EVT_MENU, self.OnExit, exitItem)
        #self.Bind(wx.EVT_BUTTON, self.onClearData, self.testPanel.configPanel.ClearDataButton)
        self.Bind(wx.EVT_BUTTON, self.onCloseConfigPanel, self.configPanel.CloseButton)
        self.Bind(wx.EVT_BUTTON, self.onOpenConfigPanel, self.testPanel.buttonStatus.ConfigPanelButton)
        # self.Bind(wx.EVT_BUTTON, self.onClearThreshhold, self.testPanel.configPanel.ClearThreshholdEvent)
        # functions

    def comReset(self, event):
        serialReset(self)

    def onClearThreshhold(self, event):
        ThreshholdFlag = False

    def OnTest(self, event):  # bring up test panel
        self.testPanel.Show()
        self.configPanel.Hide()

    def onCloseConfigPanel(self, event):  # bring up test panel
        self.testPanel.Show()
        self.configPanel.Hide()

    def OnConfig(self, event):  # bring up event panel
        self.configPanel.Show()
        self.testPanel.Hide()

    def onOpenConfigPanel(self, event):  # bring up event panel
        self.configPanel.Show()
        self.testPanel.Hide()

    def getFolderSize(self, folder):  # Get size of a folder
        total_size = os.path.getsize(folder)
        for item in os.listdir(folder):
            itempath = os.path.join(folder, item)
            if os.path.isfile(itempath):
                total_size += os.path.getsize(itempath)
            elif os.path.isdir(itempath):
                total_size += getFolderSize(itempath)
        return total_size

if __name__ == '__main__':
    # When this module is run (not imported) then create the app, the
    # frame, show it, and start the event loop.
    app = wx.App()
    global serialLock
    serialLock = False
    FTDISerial = serial.Serial()
    mainform = MainFrame(None, title='Main')
    # #serialInit(mainform)
    # threadIn = InThread(2, "threadIn", mainform)
    # threadIn.start()
    mainform.Show()
    # connection = have_internet()
    # if connection == False:
    #     wx.MessageBox("No Internet Connection")
    # wx.MessageBox("To ensure that data aquisition is not cut off, Please make sure that your computer is not set to sleep after idling. (Settings => Power and Sleep)")
    app.MainLoop()