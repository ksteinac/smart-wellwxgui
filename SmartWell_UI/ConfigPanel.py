'''
Copied from Bassem's Event panel and edited
Needs to be turned into what was the frame to select color coded configuration
'''
import sys
import glob
import wx
import serial       #need to install pyserial library
import time         #builtin library
from ConfigSub import ConfigSub

class ConfigPanel(wx.Panel):
    """"""
    #----------------------------------------------------------------------
    def __init__(self, parent, size, FTDISerial):
        """Constructor"""
        wx.Panel.__init__(self, parent=parent, size = size)
        self.FTDISerial = FTDISerial

        self.Conf1 = ConfigSub(self, (400,300), FTDISerial)
        self.Conf1.SetBackgroundColour("Red")
        self.Conf2 = ConfigSub(self, (500,400), FTDISerial)
        self.Conf2.SetBackgroundColour("Green")


        #Attempting to get a bitmap of a circle setup as a button for smart-well config chooser
        png = wx.Bitmap('circle.png', wx.BITMAP_TYPE_PNG)
        self.well1 = wx.BitmapButton(self, id = wx.ID_ANY, bitmap = png,
         size = (png.GetWidth()+10, png.GetHeight()+10))
        #self.well1.Bind(wx.EVT_BUTTON, self.OnClicked)
        self.well1.SetBackgroundColour('red')#plan to set as variable for inheritability from config panels

        self.ConfigText = wx.StaticText(self, label="Configuration: ")

        #list for storing configuration files
        self.configFiles = []


        self.configText = wx.StaticText(self, label="Saved Configurations")
        self.configComboBox = wx.ComboBox(self, style=wx.TE_PROCESS_ENTER)
        # self.Bind(wx.EVT_TEXT_ENTER, self.onNewConfig, self.configComboBox)
        self.loadButton = wx.Button(self, label="Load Configuration", style=0, size=(300,150))

       # self.Bind(wx.EVT_BUTTON, self.onLoadButton, self.loadButton)


        self.saveButton = wx.Button(self, label="Save Configuration", style=0, size=(300,150))
        self.CloseButton = wx.Button(self, label="Continue", style=0, size=(300,150))

        #box for loaded configurations dropdown
        self.hbox0 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox0.Add(self.configText, flag=wx.ALL)
        self.hbox0.AddSpacer(10)
        self.hbox0.Add(self.configComboBox, flag=wx.ALL)


        self.hbox1 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox1.AddSpacer(50)
        self.hbox1.Add(self.saveButton, flag=wx.ALL)
        self.hbox1.AddSpacer(10)

        self.hbox2 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox2.AddSpacer(50)
        self.hbox2.Add(self.loadButton, flag=wx.ALL)
        self.hbox2.AddSpacer(10)

        self.hbox3 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox3.AddSpacer(50)
        self.hbox3.Add(self.CloseButton, 0, flag=wx.ALL)
        self.hbox3.AddSpacer(10)

        self.vbox0 = wx.BoxSizer(wx.VERTICAL)
        self.vbox0.AddSpacer(20)
        self.vbox0.Add(self.well1, 0, flag=wx.ALL)
        self.vbox0.AddSpacer(10)
        self.vbox0.Add(self.hbox0, 1, flag=wx.ALL)
        self.vbox0.AddSpacer(10)
        self.vbox0.Add(self.Conf1, 1, flag=wx.ALIGN_LEFT | wx.ALIGN_TOP)
        self.vbox0.AddSpacer(20)

        self.vbox1 = wx.BoxSizer(wx.VERTICAL)
        self.vbox1.AddSpacer(20)
        self.vbox1.Add(self.hbox1, 0, flag=wx.ALL|wx.ALIGN_RIGHT)
        self.vbox1.AddSpacer(20)
        self.vbox1.Add(self.hbox2, 0, flag=wx.ALL)
        self.vbox1.AddSpacer(20)
        self.vbox1.Add(self.hbox3, 0, flag=wx.ALL)
        self.vbox1.AddSpacer(100)
        self.vbox1.Add(self.Conf2, 1, flag = wx.ALIGN_LEFT)



        self.hbox10 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox10.AddSpacer(20)
        self.hbox10.Add(self.vbox0, 0, flag=wx.ALL)
        self.hbox10.Add(self.vbox1, 0, flag=wx.ALL)

        self.SetSizerAndFit(self.hbox10)
        self.hbox10.Fit(self)

    # removed save button for config for now

    # Configuration save removed for now
    # saves the config values in a file

    # Load configure removed for now
    # run when load button is pressed
    # load config file removed with exceptions
    # class Event:
    #     def __init__(self, EventName, EventTime, EventDiscription):
    #         self.Name = EventName
    #         self.Time = EventTime
    #         self.Discription = EventDiscription
    #         self.Triggered = False
    #
    # def onGrabEvent(self, Config):
    #     for Event in self.Events:
    #         if Event.Name == self.ConfigComboBox.GetValue():
    #             self.textboxEventTime.SetValue(Event.Time)
    #             self.textboxEventDiscription.SetValue(Event.Discription)
    #             return
    #
    # def onAddEvent(self, Config):
    #     for Event in self.Events:
    #         if Event.Name == self.ConfigComboBox.GetValue():
    #             Event.Time = self.textboxEventTime.GetValue()
    #             Event.Discription = self.textboxEventDiscription.GetValue()
    #             return
    #     self.Events.insert(0, self.Event(self.ConfigComboBox.GetValue(), self.textboxEventTime.GetValue(), self.textboxEventDiscription.GetValue()))
    #     self.ConfigComboBox.Insert(self.ConfigComboBox.GetValue(), 0)
    #
    # def onDeleteEvent(self, Config):
    #     i = 0
    #     for Event in self.Events:
    #         if Event.Name == self.ConfigComboBox.GetValue():
    #             break
    #         i += 1
    #     self.Events.pop(i)
    #     self.ConfigComboBox.Delete(i)
    #     self.textboxEventTime.SetValue("")
    #     self.textboxEventDiscription.SetValue("")
