import wx
'''
This is a panel intended to display the 1,6,12,24,48, or 96 wells and make them each selectable buttons to populate a listbox with available sensors.
'''

class WellSelectPanel(wx.Panel):
    def __init__(self, parent, size):
        """Constructor"""
        wx.Panel.__init__(self, parent=parent, size=size, style=wx.SUNKEN_BORDER)
        # Console Stuff
        self.FTDISerial = FTDISerial
        # Set up Console textboxes and buttons
        self.outputTextbox = wx.TextCtrl(self, value="", pos=(25, 25),
                                         size=(self.GetSize().GetWidth() - 100, self.GetSize().GetHeight() - 100),
                                         style=wx.TE_MULTILINE | wx.TE_READONLY)
        self.inputTextbox = wx.TextCtrl(self, value="", pos=(25, self.outputTextbox.GetSize().GetHeight() + 50),
                                        size=(self.GetSize().GetWidth() - 100, 20), style=wx.TE_PROCESS_ENTER)
        self.inputButton = wx.Button(self, label="Enter", pos=(
            self.inputTextbox.GetSize().GetWidth() + 25, self.outputTextbox.GetSize().GetHeight() + 50), size=(50, 20),
                                     style=0)
       # self.Bind(wx.EVT_TEXT_ENTER, self.OnEnter, self.inputTextbox)
        #self.Bind(wx.EVT_BUTTON, self.OnEnter, self.inputButton)


