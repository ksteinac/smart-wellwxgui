import wx
''''''

class ConfigSub(wx.Panel):
    def __init__(self, parent, size, FTDISerial):
        wx.Panel.__init__(self, parent=parent, size=size)
        # Console Stuff
        self.FTDISerial = FTDISerial
        #
        #self.well = wx.StaticText(self, label="Config 1")

        #self.well = wx.StaticText(self, label="Single Well")

        # Oxygen

        self.oxygenText = wx.StaticText(self, label="Oxygen Sensor:")
        self.oxygenEnableText = wx.StaticText(self, label="Enable:")
        self.checkboxOxEn = wx.CheckBox(self, label="")
        self.oxygenActivationVoltageText = wx.StaticText(self, label="Activation Voltage: (V)")
        self.textboxOxCh1ActivationVoltage = wx.TextCtrl(self, value="", size=(75, 20))

        # PH

        self.PHText = wx.StaticText(self, label="PH Sensor:")
        self.PHEnableText = wx.StaticText(self, label="Enable:")
        self.checkboxPHEn = wx.CheckBox(self, label="")

        #Glucose
        self.GlucoseText = wx.StaticText(self, label="Glucose Sensor:")
        self.GlucoseEnableText = wx.StaticText(self, label="Enable:")
        self.checkboxGlucoseEn = wx.CheckBox(self, label="")

        #Lactate
        self.LactateText = wx.StaticText(self, label="Lactate Sensor:")
        self.LactateEnableText = wx.StaticText(self, label="Enable:")
        self.checkboxLactateEn = wx.CheckBox(self, label="")

        # Oxygen

        self.hbox2 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox2.AddSpacer(10)
        self.hbox2.Add(self.oxygenEnableText, flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL)
        self.hbox2.AddSpacer(30)
        self.hbox2.Add(self.checkboxOxEn, flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL)

        self.hbox3 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox3.AddSpacer(10)
        self.hbox3.Add(self.oxygenActivationVoltageText, flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL)
        self.hbox3.AddSpacer(14)
        self.hbox3.Add(self.textboxOxCh1ActivationVoltage, flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL)
        self.hbox3.AddSpacer(10)

        # PH

        self.hbox12 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox12.AddSpacer(10)
        self.hbox12.Add(self.PHEnableText, flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL)
        self.hbox12.AddSpacer(30)
        self.hbox12.Add(self.checkboxPHEn, flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL)
        self.hbox12.AddSpacer(70)

        #Glucose

        self.hbox13 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox13.AddSpacer(10)
        self.hbox13.Add(self.GlucoseEnableText, flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL)
        self.hbox13.AddSpacer(30)
        self.hbox13.Add(self.checkboxGlucoseEn, flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL)
        self.hbox13.AddSpacer(30)

        # Lactate

        self.hbox14 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox14.AddSpacer(10)
        self.hbox14.Add(self.LactateEnableText, flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL)
        self.hbox14.AddSpacer(30)
        self.hbox14.Add(self.checkboxLactateEn, flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL)
        self.hbox14.AddSpacer(30)

        # adding vertical alignment using vertical box sizers
        self.vbox1 = wx.BoxSizer(wx.VERTICAL)
        self.vbox1.Add(self.PHText, 0, flag=wx.ALL)
        self.vbox1.AddSpacer(10)
        self.vbox1.Add(self.hbox12, 0, flag=wx.ALL)
        self.vbox1.AddSpacer(10)
        self.vbox1.Add(self.oxygenText, 0, flag=wx.ALL)
        self.vbox1.AddSpacer(10)
        self.vbox1.Add(self.hbox2, 0, flag=wx.ALL)
        self.vbox1.AddSpacer(10)
        self.vbox1.Add(self.hbox3, 0, flag=wx.ALL)
        self.vbox1.AddSpacer(10)


        self.vbox2 = wx.BoxSizer(wx.VERTICAL)
        self.vbox2.Add(self.GlucoseText, 0, flag=wx.ALL)
        self.vbox2.AddSpacer(10)
        self.vbox2.Add(self.hbox13, 0, flag=wx.ALL)
        self.vbox2.AddSpacer(10)
        self.vbox2.Add(self.LactateText, 0, flag=wx.ALL)
        self.vbox2.AddSpacer(10)
        self.vbox2.Add(self.hbox14, 0, flag=wx.ALL)
        self.vbox2.AddSpacer(10)

        #setting up side by side vboxes
        self.hbox1 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox1.Add(self.vbox1, 0, flag=wx.ALL)
        self.hbox1.AddSpacer(10)
        self.hbox1.Add(self.vbox2, 0, flag=wx.ALL)

        self.SetSizerAndFit(self.hbox1)
        self.hbox1.Fit(self)