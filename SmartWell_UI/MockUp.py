'''
Copied from Bassem's and then edited for context and construction for the main run page
This panel sets up most of the text boxes and things and organizes the display.
'''
import sys
import glob
import os
import wx
import serial       #need to install pyserial library
import time         #builtin library
import threading    #used to allow user input without stalling processor

from GraphPanel import GraphPanel
#from ViewWindow import ViewWindow

from ButtonsStatus import ButtonsStatus
class TestPanel(wx.Panel):
    """"""
    #----------------------------------------------------------------------
    def __init__(self, parent, size, FTDISerial):
        """Constructor"""
        wx.Panel.__init__(self, parent=parent, size = size)
        #Console Stuff
        self.FTDISerial = FTDISerial

        #This entire portion is just arranging items on the screen.

        self.buttonStatus = ButtonsStatus(self, self.GetSize())#, FTDISerial)
        #self.configSub = ConfigSub(self, self.GetSize(), FTDISerial)

        # self.Bind(wx.EVT_BUTTON, self.onSaveButton, self.buttonStatus.saveButton)
        # self.Bind(wx.EVT_BUTTON, self.onStartButton, self.buttonStatus.startButton)

        self.running = False
        self.timeMin = 0
        self.graphPanel = GraphPanel(self)


        self.backupFilePath = ""
        self.FilePathText = wx.StaticText(self, label="Data Save Location: ")
        self.textboxFilePath = wx.TextCtrl(self, value=self.backupFilePath, size=(300,20))
        self.FilePathButton = wx.Button(self, label="Browse", style=0)

        self.timeText = wx.StaticText(self, label="Time Running: ")
        self.totalDataSizeText = wx.StaticText(self, label="Data Size: ")

        self.Bind(wx.EVT_BUTTON, self.onFilePathButton, self.FilePathButton)






        self.Ch1 = wx.StaticText(self, label="Well 1")

        self.hbox1 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox1.AddSpacer(100)


        # self.hbox5 = wx.BoxSizer(wx.HORIZONTAL)
        # self.hbox5.AddSpacer(10)
        # self.hbox5.Add(self.configText, flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL)
        # self.hbox5.AddSpacer(10)
        # self.hbox5.Add(self.configComboBox, flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL)
        # self.hbox5.AddSpacer(10)
        # self.hbox5.Add(self.loadConfigButton, flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL)



        self.hbox6 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox6.AddSpacer(80)
        self.hbox6.Add(self.Ch1, flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL)


        self.vbox2 = wx.BoxSizer(wx.VERTICAL)
        self.vbox2.AddSpacer(10)
        self.vbox2.Add(self.hbox6, 0, flag=wx.ALIGN_LEFT | wx.TOP)


        self.hbox8 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox8.AddSpacer(10)
        self.hbox8.Add(self.FilePathText, flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL)
        self.hbox8.AddSpacer(10)
        self.hbox8.Add(self.textboxFilePath, flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL)
        self.hbox8.AddSpacer(10)
        self.hbox8.Add(self.FilePathButton, flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL)

        self.hbox10 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox10.AddSpacer(10)
        self.hbox10.Add(self.timeText, 0, flag=wx.ALIGN_LEFT | wx.TOP)
        self.hbox10.AddSpacer(250)
        self.hbox10.Add(self.totalDataSizeText, 0, flag=wx.ALIGN_LEFT | wx.TOP)
        self.hbox10.AddSpacer(10)

        self.vbox4 = wx.BoxSizer(wx.VERTICAL)
        self.vbox4.AddSpacer(10)
        self.vbox4.Add(self.hbox8, 0, flag=wx.ALIGN_LEFT | wx.TOP)
        self.vbox4.AddSpacer(10)
        self.vbox4.Add(self.hbox10, 0, flag=wx.ALIGN_LEFT | wx.TOP)

        self.hbox2 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox2.AddSpacer(10)
        self.hbox2.Add(self.vbox2, flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL)
        self.hbox2.AddSpacer(30)
        self.hbox2.Add(self.vbox4, flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL)

        self.vbox1 = wx.BoxSizer(wx.VERTICAL)
        self.vbox1.AddSpacer(10)
        self.vbox1.Add(self.hbox2, 0, flag=wx.ALIGN_LEFT | wx.TOP)
        self.vbox1.AddSpacer(250)
        self.vbox1.Add(self.graphPanel, 1, flag=wx.ALIGN_LEFT | wx.ALIGN_TOP)
        self.vbox1.AddSpacer(10)

        # self.vbox1.AddSpacer(10)
        # self.vbox1.Add(self.hbox4, 1, flag=wx.ALL)

        self.vbox3 = wx.BoxSizer(wx.VERTICAL)
        # self.vbox3.AddSpacer(10)
        # self.vbox3.Add(self.hbox5, 0, flag=wx.ALIGN_LEFT | wx.TOP)
        # self.vbox3.AddSpacer(10)
        self.vbox3.Add(self.buttonStatus, 0, flag=wx.ALIGN_LEFT | wx.TOP)

        self.hbox2 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox2.Add(self.vbox1, 0, flag=wx.ALL)
        self.hbox2.AddSpacer(10)
        self.hbox2.Add(self.vbox3, 0, flag=wx.ALL | wx.ALIGN_TOP)

        self.SetSizerAndFit(self.hbox2)
        self.hbox2.Fit(self)
        test = 1

        # this chunk sets up the config and sets defualt values in the text boxes.
        # while(test):
        #     try:
        #         file = open("BioAnalysisChamberConfig.ChamberInfo", mode = "r")
        #         for line in file:
        #             self.configFiles.append(line.rstrip()[0:-14])
        #             self.configComboBox.Insert(line.rstrip()[0:-14], 0)
        #             #print(line)
        #
        #         self.configComboBox.SetSelection(self.configComboBox.GetCount() - 1)
        #         self.LoadConfigFile(self.configFiles[0] + ".ChamberConfig")
        #         test = 0
        #         file.close()
        #     except Exception as e:
        #         file = open("BioAnalysisChamberConfig.ChamberInfo", encoding = 'utf-8', mode = "w")
        #         file.write("default.ChamberConfig\n")
        #         file.close()
        #         #print(str(e))


  # removed onNewconfig to start
        # called when a new config is made



    def onStartButton(self, event): # called on start button being pressed.
        if(self.buttonStatus.startButton.GetLabel() == "Start"):
            self.running = True
            self.SaveConfig()
            fileName =  self.configComboBox.GetValue() + ".ChamberConfig"
            self.buttonStatus.consolePanel.outputTextbox.AppendText("Loading Configuration.\n")
            if fileName == ".ChamberConfig":
                self.buttonStatus.consolePanel.outputTextbox.AppendText("No Configuration Selected\n")
                return
            file = open(fileName, encoding = 'utf-8', mode = "r")
            self.FTDISerial.write(("Config\n").encode()) # Send the config command to the board
            threading.Event().wait(2)
            configString = ""
            for line in file:
                # self.FTDISerial.write((file).encode())
                configString += line[0:-1] + ":"
                self.buttonStatus.consolePanel.outputTextbox.AppendText(".")
            file.close()
            configString += "\n"
            self.FTDISerial.write((configString).encode())
            self.buttonStatus.consolePanel.outputTextbox.AppendText("\n")
            self.buttonStatus.startButton.SetLabel("Stop")
        else:
            # self.ack = False
            # while(self.ack == False):
            self.running = False
            self.FTDISerial.write(("Stop").encode())
            self.buttonStatus.startButton.SetLabel("Start")



    def onFilePathButton(self, event): # set up file path
        file_choices = "CSV (*.csv)|*.csv"

        with wx.DirDialog (None,
                                "Choose input directory",
                                "",
                                wx.DD_DEFAULT_STYLE | wx.DD_DIR_MUST_EXIST) as fileDialog:

            if fileDialog.ShowModal() == wx.ID_CANCEL:
                return     # the user changed their mind

            # save the current contents in the file
            self.backupFilePath = fileDialog.GetPath()
            self.textboxFilePath.SetValue(self.backupFilePath)


     # load config file removed with exceptions
