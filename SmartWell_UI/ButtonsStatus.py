'''
This file is used to create the configuration text box format. It is purely display and all functionallity is done in the MockUp.py file
'''
import sys
import glob
import wx
import serial       #need to install pyserial library
import time         #builtin library
from ConsolePanel import ConsolePanel


class ButtonsStatus(wx.Panel):
    """"""
    #----------------------------------------------------------------------
    def __init__(self, parent, FTDISerial):
        """Constructor"""
        wx.Panel.__init__(self, parent=parent)
        #Console Stuff
        self.FTDISerial = FTDISerial
        self.well = wx.StaticText(self, label="Single Well")




        self.stopButton = wx. Button(self, label="Stop", style=0, size=(200,100))
        self.startButton = wx.Button(self, label="Start", style=0, size=(200,100))
        self.ConfigPanelButton = wx.Button(self, label="Config", style=0, size=(200,100))

        # self.stopButton = wx.Button(self, label="Stop", pos=(600, self.GetSize().GetHeight() - 300), style=0)

        self.consolePanel = ConsolePanel(self, (500,200), FTDISerial)

        self.hbox0 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox0.AddSpacer(175)
        self.hbox0.Add(self.well, flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL)
        self.hbox0.AddSpacer(30)



        self.vbox14 = wx.BoxSizer(wx.VERTICAL)
        self.vbox14.Add(self.startButton, flag=wx.ALL | wx.ALIGN_CENTER_HORIZONTAL)
        self.vbox14.AddSpacer(10)
        self.vbox14.Add(self.stopButton, flag=wx.ALL | wx.ALIGN_CENTER_HORIZONTAL)
        self.vbox14.AddSpacer(10)
        self.vbox14.Add(self.ConfigPanelButton, flag=wx.ALL | wx.ALIGN_CENTER_HORIZONTAL)
        self.vbox14.AddSpacer(10)


        self.hbox1 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox1.AddSpacer(10)
        self.hbox1.Add(self.hbox0, 0, flag=wx.ALL)
        self.hbox1.AddSpacer(10)
        self.hbox1.Add(self.vbox14, 0, flag=wx.ALL)


        self.vbox2 = wx.BoxSizer(wx.VERTICAL)
        self.vbox2.Add(self.hbox1, 0, flag=wx.ALL)
        self.vbox2.AddSpacer(10)
        self.vbox2.Add(self.consolePanel, 0, flag=wx.ALL)

        self.SetSizerAndFit(self.vbox2)
        self.vbox2.Fit(self)
