'''This is straight out of Bassem's, will not likely be used at all.
It makes for a convenient launch point for the Status Window which will be populated with some data from a serial console'''

'''
This module creates a console panel that can be placed inside of a larger panel.
'''
import sys
import glob
import wx
import serial       #need to install pyserial library
import time         #builtin library
import threading    #used to allow user input without stalling processor

class ConsolePanel(wx.Panel):
    """"""
    #----------------------------------------------------------------------
    def __init__(self, parent, size, dueSerial):
        """Constructor"""
        wx.Panel.__init__(self, parent=parent, size = size, style=wx.SUNKEN_BORDER)
        #Console Stuff
        self.dueSerial = dueSerial
        # Set up Console textboxes and buttons
        self.outputTextbox = wx.TextCtrl(self, value="", pos=(25,25), size=(self.GetSize().GetWidth()-100,self.GetSize().GetHeight()-100), style=wx.TE_MULTILINE|wx.TE_READONLY)
        self.inputTextbox = wx.TextCtrl(self, value="", pos=(25, self.outputTextbox.GetSize().GetHeight() + 50), size=(self.GetSize().GetWidth()-100,20), style=wx.TE_PROCESS_ENTER)
        self.inputButton = wx.Button(self, label="Enter", pos=(self.inputTextbox.GetSize().GetWidth() + 25, self.outputTextbox.GetSize().GetHeight() + 50), size=(50,20), style=0)
        self.Bind(wx.EVT_TEXT_ENTER, self.OnEnter, self.inputTextbox)
        self.Bind(wx.EVT_BUTTON, self.OnEnter, self.inputButton)


    def OnEnter(self, event): #Send command on enter
        try:
            inStr = self.inputTextbox.GetValue()
            self.outputTextbox.AppendText(inStr + "\n")
            self.inputTextbox.Clear()
            if(inStr == "Port" or inStr == "port"):
                self.outputTextbox.AppendText("\n" + self.dueSerial.name + "\n\n")
            elif(inStr == "Clear" or inStr == "clear"):
                self.outputTextbox.Clear()
            else:
                self.dueSerial.write((inStr + '\n').encode())
        except Exception as e:
            self.outputTextbox.AppendText(str(e) + " from OnEnter ConsolePanel\n")
